<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

use App\Models\Blog;

class BlogFactory extends Factory
{

    protected $model = Blog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'category_id' => $this->faker->numberBetween(1, 3),
            'title' =>  $this->faker->word(),
            'body' =>  $this->faker->paragraph(),
            'user_id' => $this->faker->numberBetween(1, 3),
        ];
    }
}
