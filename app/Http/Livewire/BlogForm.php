<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Blog;
use App\Models\Category;

class BlogForm extends Component
{

    public $blogId;
    public $category_id;
    public $title;
    public $body;

    public function submitForm()
    {
        if($this->blogId===0){
            $blog = new Blog();
        }else{
            $blog = Blog::where(['id' => $this->blogId])->first();
        }

        $user_id = auth()->user()->id;

        $blog->category_id = $this->category_id;
        $blog->title = $this->title;
        $blog->body = $this->body;
        $blog->user_id = $user_id;

        $blog->save();

        session()->flash('flash.banner', "¡Blog guardado! ");

        return $this->redirect('/my-blogs');
    }
    

    public function render()
    {
        $categories = Category::all(); 

        return view('livewire.blog-form', ['categories' => $categories]);
    }
}
