<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CreateCategoryModal extends Component
{

    public $showCategoryModal= false;

    public function openCategoryModal(){
        $this->showCategoryModal = true;
    }

    public function render()
    {
        return view('livewire.create-category-modal');
    }
}
