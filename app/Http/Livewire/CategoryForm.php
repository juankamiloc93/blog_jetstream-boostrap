<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Category;

class CategoryForm extends Component
{

    public $name;
    public $description;

    public function submitForm()
    {
        $category = new Category(); 

        $category->name = $this->name;
        $category->description = $this->description;
 
        $category->save();

        session()->flash('flash.banner', "Categoría guardada! ");

        return $this->redirect('/my-blogs');
    }

    public function render()
    {
        return view('livewire.category-form');
    }
}
