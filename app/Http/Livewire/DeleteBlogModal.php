<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Blog;

class DeleteBlogModal extends Component
{

    public $confirmingBlogDeletion= false;
    public $blogIdBeingDeleted;
   

    public function deleteBlog(){
        $this->confirmingBlogDeletion= false;        

        $blog = Blog::where(['id' => $this->blogIdBeingDeleted])->first();
        $blog->delete();

        session()->flash('flash.banner', "¡Blog eliminado! " .  $this->blogIdBeingDeleted);

        return $this->redirect('/my-blogs');
    }

    public function confirmBlogDeletion()
    {
        $this->confirmingBlogDeletion = true;      
    }

    public function render()    
    {
       
        return view('livewire.delete-blog-modal', ['id' => $this->blogIdBeingDeleted]);
    }
}
