<?php

namespace App\Http\Livewire;

use Livewire\Component;

class BlogModal extends Component
{
  
    public $blog;

    public $showBlogModal= false;

    public function openBlogModal(){
        $this->showBlogModal = true;
    }

    public function render()
    {
        if(!$this->blog){
           return view('livewire.create-blog-modal');
        }

        return view('livewire.blog-modal', [           
            'blog' => $this->blog
        ]);
    }
}
