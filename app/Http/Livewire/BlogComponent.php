<?php

namespace App\Http\Livewire;

use Livewire\Component;

use App\Models\Blog;

class BlogComponent extends Component
{
    public function render()
    {

        $blogs = Blog::with('category')->get();

        return view('livewire.blog', ['blogs' => $blogs]);
    }
}
