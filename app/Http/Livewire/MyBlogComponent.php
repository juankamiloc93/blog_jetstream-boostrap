<?php

namespace App\Http\Livewire;

use Livewire\Component;



use App\Models\Blog;

class MyBlogComponent extends Component
{
    public function render()
    {
        $user_id = auth()->user()->id;
        $blogs = Blog::with('category')->where(['user_id' => $user_id])->get();        
        return view('livewire.my-blog-component', ['blogs' => $blogs]);
    }
}
