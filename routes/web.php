<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', App\Http\Livewire\BlogComponent::class)
    ->middleware(['auth:sanctum',  config('jetstream.auth_session'), 'verified'])
    ->name('blogs');

Route::get('/blogs', App\Http\Livewire\BlogComponent::class)
    ->middleware(['auth:sanctum',  config('jetstream.auth_session'), 'verified'])
    ->name('blogs');

Route::get('/my-blogs', App\Http\Livewire\MyBlogComponent::class)
    ->middleware(['auth:sanctum',  config('jetstream.auth_session'), 'verified'])
    ->name('my-blogs');


Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
});
