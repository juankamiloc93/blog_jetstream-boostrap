<x-slot name="header">
    <div class="d-flex justify-content-between">
        <h2>Mis blogs</h2>
        <div class="d-flex">
            <livewire:blog-modal                    
                :wire:key="'blog-0'"
            />
            <livewire:create-category-modal                    
                :wire:key="'cat-0'"
            />
           
        </div>
    </div>   
</x-slot>

<div class="row">
    @foreach($blogs as $blog)
        <div class="col-md-6 col-lg-4 col-xl-3 p-3">
            <div class="card">
                <img class="card-img-top" src="https://images.unsplash.com/photo-1500964757637-c85e8a162699?auto=format&fit=crop&q=80&w=1806&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="Card image cap">
                <div class="card-body">                   
                    <h5 class="card-title">{{$blog->title}}</h5>
                    <p class="card-text">{{$blog->category->name}}</p>                    
                    <div class="d-flex">                        
                        <livewire:blog-modal                            
                            :blog="$blog"
                            :wire:key="$blog->id"
                        />
                        <livewire:delete-blog-modal                       
                            :blogIdBeingDeleted="$blog->id"
                            :wire:key="$blog->id"
                        />
                    </div>                    
                </div>
            </div>
        </div>
    @endforeach
</div>
