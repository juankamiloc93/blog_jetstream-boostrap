<div class="m-1">
    <x-jet-button wire:click="openCategoryModal()" :wire:key="'cat-0'">
        Crear categoría
    </x-jet-button>
    <x-jet-dialog-modal wire:model="showCategoryModal" :id="'modal-cat-' . 0">
        <x-slot name="title">
            Crear categoría
        </x-slot>
        <x-slot name="content">
            <livewire:category-form/>
        </x-slot>
        <x-slot name="footer">
             
        </x-slot>   
    </x-jet-dialog-modal>
</div>

