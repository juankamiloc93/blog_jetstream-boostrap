<form wire:submit.prevent="submitForm">   
    <div class="form-group mt-2">
        <label for="title">Nombre</label>
        <input type="text" wire:model="name" id="neame" class="form-control">
    </div>
    <div class="form-group mt-2">
        <label for="title">Descripción</label>
        <input type="text" wire:model="description" id="description" class="form-control">
    </div>

    <button type="submit" class="btn btn-success mt-2 form-control">Guardar</button>
</form>
