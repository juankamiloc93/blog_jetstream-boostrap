<x-slot name="header">
    <h2>Todos los blogs</h2>
</x-slot>

<div class="row">
    @foreach($blogs as $blog)
        <div class="col-md-6 col-lg-4 col-xl-3 p-3">
            <div class="card">
                <img class="card-img-top" src="https://images.unsplash.com/photo-1500964757637-c85e8a162699?auto=format&fit=crop&q=80&w=1806&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="Card image cap">
                <div class="card-body">
                    <h5 class="card-title">{{$blog->title}}</h5>
                    <p class="card-text">{{$blog->category->name}}</p>                                  
                </div>
            </div>
        </div>
    @endforeach
</div>