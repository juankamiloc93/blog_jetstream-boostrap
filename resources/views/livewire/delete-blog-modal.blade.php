<div class="m-1">
    <x-jet-danger-button wire:click="confirmBlogDeletion()" :wire:key="$id">
        Eliminar
    </x-jet-danger-button>
    <x-jet-confirmation-modal wire:model="confirmingBlogDeletion" :id="'confirm-modal-' . $id">
        <x-slot name="title">
            Eliminar blog
        </x-slot>
        <x-slot name="content">
            ¿Esta seguro que quiere eliminar el blog?
        </x-slot>
        <x-slot name="footer">
            <x-jet-secondary-button class="ml-3" wire:click="$toggle('confirmingBlogDeletion')" wire:loading.attr="disabled">
                Cancelar
            </x-jet-secondary-button>
            <x-jet-danger-button class="ml-3" wire:click="deleteBlog" wire:loading.attr="disabled">
                Eliminar
            </x-jet-danger-button>
        </x-slot>
    </x-jet-confirmation-modal>
</div>
