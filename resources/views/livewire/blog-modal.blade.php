<div class="m-1">
    <x-jet-button wire:click="openBlogModal()" :wire:key="$blog->id">
        Modificar
    </x-jet-button>
    <x-jet-dialog-modal wire:model="showBlogModal" :id="'modal-' . $blog->id">
        <x-slot name="title">
            Modificar
        </x-slot>
        <x-slot name="content">
            <livewire:blog-form
                :blogId="$blog->id"
                :category_id="$blog->category_id"
                :title="$blog->title"
                :body="$blog->body"
            />
        </x-slot>
        <x-slot name="footer">
             
        </x-slot>   
    </x-jet-dialog-modal>
</div>
