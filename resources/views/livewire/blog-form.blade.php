<form wire:submit.prevent="submitForm">   
    <div class="form-group">
        <label for="category_id">Categoría</label>
        <select wire:model="category_id" id="category_id" class="form-control">
            <option></option>
        @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
        @endforeach
        </select>        
    </div>
    <div class="form-group mt-2">
        <label for="title">Título</label>
        <input type="text" wire:model="title" id="title" class="form-control">
    </div>
    <div class="form-group mt-2">
        <label for="body">Cuerpo</label>
        <textarea wire:model="body" id="body" class="form-control">
        </textarea>
    </div>

    <button type="submit" class="btn btn-success mt-2 form-control">Guardar</button>
</form>