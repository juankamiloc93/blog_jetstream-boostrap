<div class="m-1">
    <x-jet-button wire:click="openBlogModal()" :wire:key="0">
        Crear blog
    </x-jet-button>
    <x-jet-dialog-modal wire:model="showBlogModal" :id="'modal-' . 0">
        <x-slot name="title">
            Crear
        </x-slot>
        <x-slot name="content">
            <livewire:blog-form  :blogId="0"/>
        </x-slot>
        <x-slot name="footer">
             
        </x-slot>   
    </x-jet-dialog-modal>
</div>
